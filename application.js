const http = require('http');

const app = module.export = {};

app.listen = function () {
  const server = http.createServer(this);
  return server.listen(...arguments);
}