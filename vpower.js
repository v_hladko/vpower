const merge = require('merge-descriptors');
const applicationPrototype = requie('./application.js');

module.exports = createApplication;

function createApplication() {
  const app = function (request, response, next) {

  }

  merge(app, applicationPrototype, false);

  return app;
}